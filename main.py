# uncompyle6 version 3.7.2
# Python bytecode 3.7 (3394)
# Decompiled from: Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)]
# Embedded file name: ./main.py
# Compiled at: 2020-07-04 10:14:03
# Size of source mod 2**32: 2983 bytes
"""
@Author: whitelok
@Date: 2020-06-29 16:20:34
@LastEditTime: 2020-07-04 10:14:03
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: /watermark-dev/main.py
"""
import pathlib, argparse, os, time, datetime, logging, coloredlogs
from autowatermarkremoval.solver import solve
from autowatermarkremoval.remover import remove
coloredlogs.install(level='DEBUG')
log = logging.getLogger(__name__)
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='\n            Watermark Remover v0.2\n            https://github.com/whitelok/watermark-remover\n            ')
    parser.add_argument('--mode', type=str, required=True, help='\n        train: train and get a watermark and a alpha image\n        remove: just remove watermark from a image\n        ')
    parser.add_argument('--dataset', type=str, help='\n        images path for training a watermark, just for train mode\n        ')
    parser.add_argument('--watermark_path', type=str, help='\n        load the watermark image, just for remove mode\n            ')
    parser.add_argument('--alpha_path', type=str, help='\n        load the alpha image, just for remove mode\n        ')
    parser.add_argument('--image_path', type=str, help='\n        load the image you want to remove the watermark, just for remove mode\n        ')
    parser.add_argument('--iters', type=int, default=3, help='\n        set training iteration times\n        ')
    parser.add_argument('--watermark_threshold', default=0.7, type=float, help='\n        set watermark threshold between (0 to 1)\n        ')
    parser.add_argument('--save_result', type=bool, default=True, help='\n        whether save result as images\n        ')
    options = parser.parse_args()
    date_time = datetime.datetime.strptime('2030-12-30', '%Y-%m-%d')
    if datetime.datetime.now() < date_time:
        if 'train' == options.mode:
            if not options.dataset is not None:
                raise AssertionError('need a --dataset for training.')
            else:
                srcpath = options.dataset
                destpath = os.path.join(options.dataset, 'result')
                if not os.path.isabs(srcpath):
                    srcpath = os.path.join(os.path.curdir, srcpath)
                os.path.exists(destpath) or os.mkdir(destpath)
            solve(pathlib.Path(srcpath), pathlib.Path(destpath), options.iters, options.watermark_threshold, options.save_result)
        else:
            if 'remove' == options.mode:
                if not (options.watermark_path and options.alpha_path and options.image_path):
                    raise AssertionError('need --watermark_path and --alpha_path and --image_path')
                print(pathlib.Path(options.watermark_path))
                print(pathlib.Path(options.alpha_path))
                print(pathlib.Path(options.image_path))
                remove(pathlib.Path(options.watermark_path), pathlib.Path(options.alpha_path), pathlib.Path(options.image_path))
            else:
                log.error('Unknown mode !!!')
    else:
        print("Can't use on your computer, sorry.")
        exit(0)
# okay decompiling main.pyc
